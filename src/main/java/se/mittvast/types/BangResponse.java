package se.mittvast.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BangResponse {
    @JsonProperty("ball")
    private Ball ball;
    @JsonProperty("state")
    private BallState state;

    public Ball getBall() {
        return ball;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public BallState getState() {
        return state;
    }

    public void setState(BallState state) {
        this.state = state;
    }
}
