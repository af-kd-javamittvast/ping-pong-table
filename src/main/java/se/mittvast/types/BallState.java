package se.mittvast.types;

import com.fasterxml.jackson.annotation.JsonClassDescription;

public enum BallState {
    EdgeBall,
    NetBall,
    Hit,
    Miss
}
