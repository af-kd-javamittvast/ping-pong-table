package se.mittvast.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Ball
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-22T17:46:24.891+02:00")

public class Ball {
    @JsonProperty("spin")
    private Long spin = null;

    @JsonProperty("speed")
    private Long speed = null;

    public Ball(Long speed) {
        this(speed, Long.valueOf(0));
    }

    public Ball(Long speed, Long spin) {
        this.speed = speed;
        this.spin = spin;
    }

    public Ball spin(Long spin) {
        this.spin = spin;
        return this;
    }

    /**
     * Get spin
     *
     * @return spin
     **/
    @ApiModelProperty(value = "")


    public Long getSpin() {
        return spin;
    }

    public void setSpin(Long spin) {
        this.spin = spin;
    }

    public Ball speed(Long speed) {
        this.speed = speed;
        return this;
    }

    /**
     * Get speed
     *
     * @return speed
     **/
    @ApiModelProperty(value = "")


    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ball ball = (Ball) o;
        return Objects.equals(this.spin, ball.spin) &&
                Objects.equals(this.speed, ball.speed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spin, speed);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Ball {\n");

        sb.append("    spin: ").append(toIndentedString(spin)).append("\n");
        sb.append("    speed: ").append(toIndentedString(speed)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

