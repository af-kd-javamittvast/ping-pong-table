package se.mittvast.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.mittvast.physics.PhysicsEngine;
import se.mittvast.types.Ball;
import se.mittvast.types.BangResponse;

@Service
public class BangService {

    private PhysicsEngine physicsEngine;

    @Autowired
    public BangService(PhysicsEngine physicsEngine) {
        this.physicsEngine = physicsEngine;
    }

    public BangResponse validate(Ball ball) {
        return physicsEngine.calculate(ball);
    }

}
