package se.mittvast.physics;

import org.springframework.stereotype.Component;
import se.mittvast.types.Ball;
import se.mittvast.types.BallState;
import se.mittvast.types.BangResponse;

@Component
public class PhysicsEngine {

    public BangResponse calculate(Ball ball) {
        BallState state = calculateBallState(ball.getSpeed());

        Long speed = calculateSpeed(ball.getSpeed());
        ball.setSpeed(speed);

        return new BangResponse() {
            {
                setState(state);
                setBall(ball);
            }
        };
    }

    private Long calculateSpeed(Long speed) {
        speed -= 2;
        return speed > 0 ? speed: 0;
    }

    private BallState calculateBallState(Long speed) {
        if (speed <= 1) {
            return BallState.NetBall;
        } else if (speed > 10) {
            return BallState.Miss;
        } else {
            return BallState.Hit;
        }
    }
}
