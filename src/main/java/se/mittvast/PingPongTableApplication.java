package se.mittvast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = {"se.mittvast"})
public class PingPongTableApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(PingPongTableApplication.class).run(args);
    }
}
