package se.mittvast.controllers;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import se.mittvast.api.BangApi;
import se.mittvast.api.BangService;
import se.mittvast.types.Ball;
import se.mittvast.types.BangResponse;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-22T17:46:24.891+02:00")

@Controller
public class BangController implements BangApi {

    private final BangService bangService;

    @Autowired
    public BangController(BangService bangService) {
        this.bangService = bangService;
    }

    @Override
    public ResponseEntity<BangResponse> bang(@ApiParam(value = "Information about the ball after the player striked the ball.", required = true) @Valid @RequestBody Ball ball) {
        BangResponse response = bangService.validate(ball);
        return new ResponseEntity<BangResponse>(response, HttpStatus.OK);
    }

}
