package physics;

import org.junit.Assert;
import org.junit.Test;
import se.mittvast.physics.PhysicsEngine;
import se.mittvast.types.Ball;
import se.mittvast.types.BangResponse;

public class PhysicsEngineTests {

    private PhysicsEngine physicsEngine = new PhysicsEngine();

    @Test
    public void calculateSlowBall() {
        Ball ball = new Ball((long) 1);
        BangResponse response = physicsEngine.calculate(ball);
        Assert.assertEquals(3, (long) response.getBall().getSpeed());
    }
}
