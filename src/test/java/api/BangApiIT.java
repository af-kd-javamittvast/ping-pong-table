package api;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import se.mittvast.PingPongTableApplication;
import se.mittvast.physics.PhysicsEngine;
import se.mittvast.types.Ball;
import se.mittvast.types.BangResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PingPongTableApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class BangApiIT {

    @LocalServerPort
    int port;

    private String host = "http://localhost:";

    @Test
    public void bangTest() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Ball> entity = new HttpEntity(createBall(), headers);

        String url = host + port + "/bang";

        ResponseEntity<BangResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, BangResponse.class);

        Assert.assertEquals(200, response.getStatusCode());
        Assert.assertEquals(createBall(), response.getBody().getBall());
    }

    private Ball createBall() {
        Ball ball = new Ball(22l, 22l);
        return ball;
    }
}
